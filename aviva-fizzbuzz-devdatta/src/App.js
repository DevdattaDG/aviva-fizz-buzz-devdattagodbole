import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import FizzBuzz from './components/FizzBuzz';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">          
          <h1 className="App-title">AVIVA React Assignment</h1>
        </header>
        <br />
		<FizzBuzz />
      </div>
    );
  }
}

export default App;
